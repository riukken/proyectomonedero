const app = require("express")();
const cors = require("cors");
const morgan = require("morgan");
app.use(cors())
app.use(morgan("dev"))
const stripe = require("stripe")("sk_test_GUdImCia0wXYyZ7bfIoxjVpt00asOGwNgH");

app.use(require("body-parser").text());

app.post("/charge", async (req, res) => {
  try {
    const { token } = req.body;

    
    const customer = await stripe.customers.create({
      source: token,
     
    });
    let {status} = await stripe.charges.create({
      amount: '5000',
      currency: "eur",
      customer: customer.id,
      description: "primera carga",
      payment_method_types: ['card'],
    });

    res.json({status});
  } catch (err) {
    console.log(err);
    res.status(500).end();
  }
});
  app.listen(5009, () => console.log("Listening on port 5009"));