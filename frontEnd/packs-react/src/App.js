import React, { Component } from 'react';
import { Switch, Route, Redirect } from "react-router-dom";

import Index from "views/Index.jsx";
import Login from "views/examples/Login.jsx";
import RegisterClient from "views/examples/RegisterClient.jsx";
import RegisterCompany from "views/examples/RegisterCompany.jsx";
import ProfileClient from 'views/profiles/ProfileClient.jsx';
import ProfileCompany from "views/profiles/ProfileCompany.jsx";
import ListCompanies from "views/ListCompanies";
import Geocalize from './views/GeocalizeDirection';
import LoginGoogle from './views/testLoginGoogle';
import Payment from './views/payment';
import Ofertas from './views/Ofertas';
import ProductListCompany from 'views/examples/ProductList.jsx';
import NewProduct from 'views/examples/NewProduct.jsx';
import ProductBuyList from 'views/ProductBuyList/ProductBuyList';



import PayCorrect from 'views/PagoCorrectamente';


class App extends Component {
  render() {
    return (
      <Switch>
        <Route path="/login-page" exact render={props => <Login {...props} />} />
        <Route
          path="/registerClient-page"
          exact
          render={props => <RegisterClient {...props} />}
        />
        <Route
          path="/profileCompany-page"
          exact
          render={props => <ProfileCompany {...props} />}
        />

        <Route
          path="/registerCompany-page"
          exact
          render={props => <RegisterCompany {...props} />}
        />
        <Route
          path="/profileClient-page"
          exact
          render={props => <ProfileClient {...props} />}
        />
        <Route path="/" exact render={props => <Index {...props} />} />
        <Route
          path="/listCompanies-page"
          exact
          render={props => <ListCompanies {...props} />}
        />
        <Route
          path="/geocalize"
          exact
          render={props => <Geocalize {...props} />}
        />
        <Route
          path="/login-google"
          exact
          render={props => <LoginGoogle {...props} />}
        />
        <Route
          path="/ofertas"
          exact
          render={props => <Ofertas {...props} />}
        />
        <Route
          path="/productListCompany-page"
          exact
          render={props => <ProductListCompany {...props} />}
        />
        <Route
          path="/newProduct-page"
          exact
          render={props => <NewProduct {...props} />}
        />






   
        <Route
          path="/geocalize"
          exact
          render={props => <Geocalize {...props} />}
        />
        <Route
          path="/login-google"
          exact
          render={props => <LoginGoogle {...props} />}
        />
        <Route
          path="/payment"
          exact
          render={props => <Payment {...props} />}
        />
        <Route
          path="/ofertas"
          exact
          render={props => <Ofertas {...props} />}
        />
        <Route
          path="/ProductList"
          exact
          render={props => <ProductBuyList {...props} />}
        />
        <Route
          path="/PagoCorrecto"
          exact
          render={props => <PayCorrect {...props} />}
        />

        <Redirect to="/" />
      </Switch>
    );
  }
}

export default App;
