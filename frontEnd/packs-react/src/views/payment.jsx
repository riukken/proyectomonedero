import React, { Component } from 'react';
//import {Elements, StripeProvider} from 'react-stripe-elements';
//import CheckoutForm from './CheckoutForm';

import { PayPalButton } from "react-paypal-button-v2";
import {Card, Container, Row, Col,Table  } from "reactstrap";
import DemoNavbar from "components/Navbars/DemoNavbar.jsx";
import Axios from 'axios';

class Payment extends Component {
    constructor(props) {
        super(props);
        this.state = {
             amount:'100',
             cantidad:'1',
             subtotal: '',
             consum: []
        }
        
        this.dataChange = this.handleChange.bind(this)  
    }

    componentDidMount(){
      
      this.setState({subtotal:this.state.amount})
    }
    
    handleChange = (e) => {
      console.log(e)
      this.setState({[e.target.name]:e.target.value,subtotal: this.state.amount * this.state.cantidad })
      
      console.log(this.state.subtotal)
   }
   
  //  CallbackFunction(event) {
  //   if (window.event) {
  //       if (window.event.clientX < 40 && window.event.clientY < 0) {
  //           alert("back button is clicked");
  //       }else{
  //           alert("refresh button is clicked");
  //       }
  //   }else{
  //       // want some condition here so that I can differentiate between
  //       // whether refresh button is clicked or back button is clicked.
  //   }
  // }
   
    render() { 
      
      const items = JSON.parse(localStorage.items);

      
      
      const consumos = items.map(element => {
        // const idUser = 1;
        // const idOferta = element.idOferta;
        // const fechaRegistro = element.caducidad;
        // const cantidad = element.cantidad;
        
        
        return element
        
      });
      //this.setState({consum:consumo})
      if (!consumos.length){
        return <h3>No hay nada que pagar</h3>
      } else {
      console.log(consumos[0].idOferta)
      const idUser = JSON.parse(window.localStorage.getItem("cliente")).id;
      const idOferta = consumos[0].idOferta;
      const fechaRegistro = consumos[0].caducidad;
      const cantidad = consumos[0].cantidad;
      var data = {
        idUser,
        idOferta,
        fechaRegistro,
        cantidad
      }
    }
      console.log(data)
      var total=0;
      //console.log(items)
        return (
          <>
          <DemoNavbar/>
          <main>
            
               <Container>
                 
                        <Card className="card-profile shadow mt-3 mb-5">
                        <div className="px-4">
                            <Row>
                              <Col  >
                                      <Table  className="table table-hover table-condensed table-responsive mt-2">
                                      <thead>
        <tr>
        <th style={{width:'50%',borderTop:'0px'}}>Producto</th>
							<th style={{width:'10%', borderTop:'0px'}}>Precio</th>
							<th style={{width:'8%', borderTop:'0px'}}>Cantidad</th>
							<th style={{width:'10%', borderTop:'0px'}}></th>
        </tr>
      </thead>
      <tbody style={{borderTop:'0px'}}>
      {items.map(item => (  
        <tr key={item.idOferta} style={{borderTop:'0px'}}>
          <td style={{borderTop:'0px'}}>
          <div className="row" >
									<div className="col-sm-2 hidden-xs"><img src={item.img} width="100" height="100" alt="..." className="img-responsive"/></div>
									<div className="col-sm-10">
										<h6 className="ml-5">Default</h6>
										<p className="ml-5" style={{fontSize:'12px'}}>{item.descripcion}</p>
									</div>
								</div>
          </td>
          <td style={{marginTop:'10px' ,position:'absolute',borderTop:'0px'}} data-th="Subtotal" className="text-center" onChange={this.handleChange}>{item.precio}€</td>
							<td data-th="Quantity" style={{borderTop:'0px'}}>

								<input type="number" className="form-control text-center" min="1" value={item.cantidad}  name="cantidad" onChange={this.handleChange}/>
							</td>
							<td className="actions" data-th="" style={{borderTop:'0px'}}>
								<button className="btn btn-info btn-sm" style={{marginTop:'-10px'}}><i className="fa fa-refresh"></i></button>
								<button className="btn btn-danger btn-sm mt-2"><i className="fa fa-trash-o"></i></button>								
							</td>
             
      <p style={{display:"none"}}>{total += parseInt(item.precio)}</p>
             
						</tr>
           
             ))} 
    <tr style={{marginLeft:'-800px'}}><strong>Precio Total: </strong>{total}€</tr>
      </tbody>
                    
                </Table>
              
        
      </Col>
      </Row>
      <Row className="justify-content-center">
        <Col className="order-lg-2" lg="5">
        <PayPalButton
            amount={total}
            onSuccess={(details) => {
              console.log(consumos)
              Axios.post('https://localhost:44358/api/consumos', data).then(response => {
                  console.log(response.data);
                  this.setState({
                    message: response.data
                  })
                  this.props.history.push({
                    pathname: '/profileClient-page'
                  })
                }).catch(err => {
                  console.log(err);
                  this.setState({
                    loading: false
                  })
                }) 
            }}
        options={{
          currency:"EUR",
          disableFunding: "sofort",
          clientId: "AWYcO2HHBk6aY16mNsRCJYiCjUOTzqtMkTtaUI0xbhMXV3DjbVIM9uMJMmIztysXOTbFsogsPPcdwWyy"
        }}
        
      />
        </Col>
      </Row>
      </div>
    </Card>
      </Container>
      </main>
      </>
        );
    }
}
 
export default Payment;