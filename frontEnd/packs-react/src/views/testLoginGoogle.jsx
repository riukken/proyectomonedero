import React, { Component } from 'react';
import GoogleLogin from 'react-google-login';
import { GoogleLogout } from 'react-google-login';
import {Button} from "reactstrap";




class LoginGoogle extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            userDetails: {},
            isUserLoggedIn: false
        }
        
    }

    responseGoogle = response => {
        console.log(response)
        if (response.error){
            
        } else {
        this.setState({ userDetails: response.profileObj, isUserLoggedIn: true });
        }
      };
    
      logout = () => {
        this.setState({isUserLoggedIn: false})
      };

    render() { 
        console.log(this.state.userDetails)
        
        return ( 
           <div className="App">
        {!this.state.isUserLoggedIn && (
          <GoogleLogin
            clientId="686826413117-dd78ce2i78hrthu9b68h2qdgf8v33u6v.apps.googleusercontent.com" //TO BE CREATED
            render={renderProps => (
            <div>
              <Button
                          className="btn-neutral btn-icon ml-1"
                          color="default"
                          href="#pablo"
                          
                          onClick={renderProps.onClick}
                            disabled={renderProps.disabled}
                        >
              <span className="btn-inner--icon mr-1">
              <img
                alt="..."
                src={require("assets/img/icons/common/google.svg")}
              />
            </span>
            <span className="btn-inner--text">Log in</span>
            </Button>
            </div>
            )}
            onSuccess={this.responseGoogle}
            onFailure={this.responseGoogle}
          />
        )}
        {this.state.isUserLoggedIn && (
          <div className="userDetails-wrapper">
            <div className="details-wrapper">
              <GoogleLogout
                render={renderProps => (
                  <Button
                    className="btn-neutral btn-icon ml-1"
                    onClick={renderProps.onClick}
                  >
                   <span className="btn-inner--icon mr-1">
                    <img
                        alt="..."
                        src={require("assets/img/icons/common/google.svg")}
                    />
                    </span>
                    <span className="btn-inner--text">Log Out</span>
                    {this.state.userDetails.email}
                  </Button>
                )}
                onLogoutSuccess={this.logout}
              />
            
              
            </div>
          </div>
        )}
      </div>
        );

   
    }
}
 
export default LoginGoogle;