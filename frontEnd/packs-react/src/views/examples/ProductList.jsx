import React, { Component } from "react";
import { Card, Row, Col } from "reactstrap";
//import { confirmAlert } from 'react-confirm-alert'; // Import ConfirmSubmit
// import { faFacebook, faTwitter, faInstagram } from "@fortawesome/free-brands-svg-icons";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// core components
import DemoNavbar from "components/Navbars/DemoNavbar.jsx";
import ReactModal from 'react-modal';
import {
    CardImg,
    CardBody,
    CardTitle,
    Button,
    CardText,
    FormGroup,
    Form,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Container
}
    from "reactstrap";
import axios from "axios";

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

class ProductList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            client: [],
            preview: null,
            showModal: false,
            showModal2: false,
            elementModal: {},
            dataProdoct: {},
            productvalue: {}

        };
        //this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleCloseModal2 = this.handleCloseModal2.bind(this);
        this.postData = this.postData.bind(this);
        this.DeleteItem = this.DeleteItem.bind(this);
        this.dataChange = this.dataChange.bind(this);


    }

    dataChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })

    }

    postData(event) {

        event.preventDefault()


        this.setState({
            loading: true,
            showModal: false,
        })

        const productvalue = {
            idOferta: this.state.id,
            idEmpresa: JSON.parse(window.localStorage.getItem("empresa")).id,
            descripcion: this.state.descripcion,
            precio: this.state.precio,
            cantidad: this.state.cantidad,
            caducidad: this.state.caducidad,
            img: this.state.img,
            mensaje: this.state.client.ofertas.mensajes,
            activa: this.state.activa
            // idEmpresa = objidEmpresa 
        }

        console.log(productvalue);
        console.log(this.state.idEmpresa)
        axios.put('https://localhost:44358/api/ofertas/' + productvalue.idOferta, productvalue).then(response => {
            console.log(response.data);
            this.setState({
                message: response.data
            })
            this.GetProductListById()
        }).catch(err => {
            console.log(err);
            console.log("ha salido error");
            this.setState({
                loading: false
            })
        })

    }
    handleOpenModal(element) {
        this.setState({
            showModal: true,
            id: element.idOferta,
            descripcion: element.descripcion,
            precio: element.precio,
            cantidad: element.cantidad,
            caducidad: element.caducidad,
            activa: element.activa

        });


    }

    handleOpenModal2(element) {
        this.setState({
            showModal2: true,
            id: element.idOferta,
            descripcion: element.descripcion,
            precio: element.precio,
            cantidad: element.cantidad,
            caducidad: element.caducidad,
            activa: element.activa

        });


    }
    handleCloseModal() {

        this.setState({ showModal: false });
    }
    handleCloseModal2() {

        this.setState({ showModal2: false });
    }

    DeleteItem(event) {
        event.preventDefault()
        this.setState({
            loading: true,
            showModal2: false
        })

        const productvalue = {
            idOferta: this.state.id,
            idEmpresa: JSON.parse(window.localStorage.getItem("empresa")).id,
            descripcion: this.state.descripcion,
            precio: this.state.precio,
            cantidad: this.state.cantidad,
            caducidad: this.state.caducidad,
            img: this.state.img,
            mensaje: this.state.client.ofertas.mensajes,
            nombreOferta: this.state.nombreOferta,
            activa: false
            //   idEmpresa = objidEmpresa 
        }
        console.log(productvalue)

        axios.put('https://localhost:44358/api/ofertas/' + productvalue.idOferta, productvalue).then(response => {
            console.log(response.data);
            this.setState({
                message: response.data
            })
            this.GetProductListById()
        }).catch(err => {
            console.log(err);
            console.log("ha salido error");
            this.setState({
                loading: false
            })
        })
    }

    componentDidMount() {

        this.GetProductListById();
    }

    GetProductListById() {
        //PONER ID DE LA EMPRESA QUE HA INICIADO SESION
        axios.get('https://localhost:44358/api/empresas/' + JSON.parse(window.localStorage.getItem("empresa")).id)
            .then(data => data.data)
            .then(data => {
                console.log(data)
                this.setState({
                    client: data,
                    idOfertaAux: data.ofertas.idOferta,
                    descripcion: data.ofertas.descripcion
                })

            })

    }
    render() {

        // PONER UN REDIRECT

        if (!this.state.client || !this.state.client.ofertas) {
            return <h1>Cargando....</h1>
        }


        return (
            <>
                <DemoNavbar />
                <main className="profile-page" ref="main">
                    <section className="section-profile-cover section-shaped my-0">
                        {/* Circles background */}
                        <div className="shape shape-style-1 shape-default alpha-4">
                            <span />
                            <span />
                            <span />
                            <span />
                            <span />
                            <span />
                            <span />
                        </div>
                        {/* SVG separator */}
                        <div className="separator separator-bottom separator-skew">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                preserveAspectRatio="none"
                                version="1.1"
                                viewBox="0 0 2560 100"
                                x="0"
                                y="0"
                            >
                                <polygon
                                    className="fill-white"
                                    points="2560 0 2560 100 0 100"
                                />
                            </svg>
                        </div>
                    </section>

                    <section className="section">
                        <Container fluid>
                            <Row>
                                {this.state.client.ofertas.map(element => (

                                    <Col lg="3">
                                        <Card key={element.idOferta} style={{ marginLeft: "12%", width: "350px", borderRadius: "2%", boxShadow: "3px 3px 10px #666", backgroundColor: "#222222", marginTop: "-130%", marginBottom: "140%" }}>
                                            <CardImg src={element.img} alt="Card image cap" />
                                            <CardBody >
                                                <CardTitle style={{ textAlign: "center", fontSize: "18px", color: "white" }}><h5 style={{ fontSize: "21px", color: "white" }}>{element.nombreOferta}</h5></CardTitle>
                                                <CardText style={{ marginTop: "-3%", fontSize: "fa-rotate-180px", color: "white" }}><h6 style={{ fontSize: "19px", color: "white" }}>Descripción: <h6 style={{ fontSize: "19px", color: "white" }}>{element.descripcion}</h6></h6> </CardText>
                                                <CardText style={{ marginTop: "-3%", fontSize: "fa-rotate-180px", color: "white" }}><h5 style={{ fontSize: "19px", color: "white" }}>Cantidad: {element.cantidad}</h5></CardText>
                                                <CardText style={{ marginTop: "-3%", fontSize: "fa-rotate-180px", color: "white" }}><h5 style={{ fontSize: "19px", color: "white" }}>Fecha de caducidad: {element.caducidad}</h5></CardText>
                                                {/* <CardText style={{ marginTop: "-3%", fontSize: "fa-rotate-180px", color: "white" }}><h5 style={{ fontSize: "19px", color: "white" }}>Oferta activa:  {element.activa.toString() ? 'Sí' : 'No'}</h5></CardText> */}
                                                 {/* <Button onClick={this.DeleteItem.bind(this, element.idOferta, this.element)}>Eliminar</Button> */}
                                                <Button  className="btn btn-primary" onClick={this.handleOpenModal2.bind(this, element)}>Eliminar</Button> 
                                                <Button style={{ marginLeft: "18%" }} className="btn btn-primary" onClick={this.handleOpenModal.bind(this, element)}>Modificar</Button>
                                                {/* <Button outline color="primary" style={{marginBottom: "-10px" ,marginTop: "-10px", width: "170px", borderRadius: "0", color: "white", borderColor: "white"}}>Button</Button>{''} */}
                                            </CardBody>
                                        </Card>
                                    </Col>


                                ))}




                                {/* <!-- Modal --> */}
                                <ReactModal
                                    style={customStyles}
                                    isOpen={this.state.showModal}
                                    onRequestClose={this.handleCloseModal}
                                >
                                    <Form role="form" onSubmit={this.postData}>

                                        <FormGroup className="mb-3">
                                            <InputGroup className="input-group-alternative">
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="ni ni-email-83" />
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input placeholder="Descripción" type="text" name="descripcion" value={this.state.descripcion} onChange={this.dataChange} />
                                            </InputGroup>
                                        </FormGroup>

                                        <FormGroup className="mb-3">
                                            <InputGroup className="input-group-alternative">
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="ni ni-email-83" />
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input placeholder="Precio" type="number" name="precio" value={this.state.precio} onChange={this.dataChange} />
                                            </InputGroup>
                                        </FormGroup>

                                        <FormGroup>
                                            <div style={{ fontSize: "19px" }} className=" text-muted">
                                                <small>Cantidad:</small>
                                            </div>
                                            <InputGroup className="input-group-alternative">
                                                <InputGroupAddon addonType="prepend">
                                                </InputGroupAddon>
                                                <Input style={{ textAlign: "center", color: "#8898aa !important", fontSize: "19px" }} type="number" name="cantidad" defaultValue="1" value={this.state.cantidad} onChange={this.dataChange} />
                                                {/* <Input placeholder="Cantidad" type="passwordUno" name="passwordUno" autoComplete="off" value={this.state.passwordUno} onChange={this.dataChange}/> */}
                                            </InputGroup>
                                        </FormGroup>
                                        <FormGroup className="mb-3">
                                            <div style={{ fontSize: "19px" }} className=" text-muted">
                                                <small>Caducidad:</small>
                                            </div>
                                            <InputGroup className="input-group-alternative">
                                                <InputGroupAddon addonType="prepend">
                   

                                                </InputGroupAddon>

                                                <Input type="text" name="caducidad"
                                                    value={this.state.caducidad} onChange={this.dataChange}
                                                    min="2019-01-01" max="2019-12-31" />
                                            </InputGroup>
                                        </FormGroup>

                                        <FormGroup className="mb-3">
                                            <div style={{ fontSize: "19px" }} className=" text-muted">
                                                <small>Imagen del producto:</small>
                                            </div>
                                            <InputGroup className="input-group-alternative">
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="ni ni-image" />
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input placeholder="Imagen" type="text" name="img" value={this.state.img} onChange={this.dataChange} />
                                            </InputGroup>
                                        </FormGroup>
                                        <p>{this.state.activa}</p>

                                        <div className="custom-control custom-control-alternative custom-checkbox">
                                            <input
                                                className="custom-control-input"
                                                id=" customCheckLogin"
                                                type="checkbox"
                                            />
                                        </div>
                                        <div className="text-center">
                                            <Button
                                                className="my-4"
                                                style={{backgroundColor: "#222222", color: "white"}}
                                                type="submit"
                                                value="submit"

                                            >
                                                Guardar
                            </Button>
                                            <button className="btn btn-primary my-4"
                                                color="primary"
                                                style={{backgroundColor: "#222222", color: "white"}}
                                                type="submit" value="submit" onClick={this.handleCloseModal}>Cancelar</button>
                                        </div>
                                    </Form>

                                </ReactModal>
                                {/* <!-- Modal --> */}
                                <ReactModal style={customStyles}
                                    isOpen={this.state.showModal2}
                                    onRequestClose={this.handleCloseModal2}
                                >
                                    <Form role="form" onSubmit={this.DeleteItem}>
                                        <div style={{textAlign: "center"}}><label style={{color: "#8898aa", fontSize: "23px"}}>¿Seguro que quieres borrarlo?</label><br></br>
                                            <Button
                                                className="my-4"
                                                style={{backgroundColor: "#222222", color:"white"}}
                                                type="submit"
                                                value="submit">
                                                borrar
                                            </Button>
                                            <button className="btn btn-primary my-4"
                                                color="secondary"
                                                style={{backgroundColor: "#222222", color:"white"}}
                                                type="submit" value="submit" onClick={this.handleCloseModal2}>Cancelar</button>
                                        </div>
                                    </Form>

                                </ReactModal>
                            </Row>
                        </Container>
                    </section>
                </main>
            </>
        );
    }
}

export default ProductList;