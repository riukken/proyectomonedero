/*!

=========================================================
* Argon Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import { Redirect } from 'react-router-dom';
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col,
  CustomInput,
} from "reactstrap";

// core components
import DemoNavbar from "components/Navbars/DemoNavbar.jsx";
import SimpleFooter from "components/Footers/SimpleFooter.jsx";
import axios from 'axios';


class Login extends React.Component {

  constructor(props) {
    super(props);


    const sessionClient = window.localStorage.getItem("cliente");
    const sessionCompany = window.localStorage.getItem("empresa");


    this.state = {
      email: '',
      passwordUno: '',
      clickCompany: false,
      loading: false,
      message: '',
      sessionStartedClient: sessionClient,
      sessionStartedCompany: sessionCompany
      //implementar company
    };



    this.postData = this.postData.bind(this);
    this.dataChange = this.dataChange.bind(this);
    this.clickCompany = this.clickCompany.bind(this);
  }

  dataChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    })

  }

  postData(event) {

    event.preventDefault()

    const email = this.state.email;
    const passwordUno = this.state.passwordUno;

    this.setState({
      loading: true
    })

    const data = {
      email,
      passwordUno,
      mensaje: ''
    }

    console.log(data);

    // HACER SCRIPT PARA DIFERENCIAR MAYUSCULAS Y MINUSCULAS

    var url
    if (this.state.clickCompany === false) {
      url = 'https://localhost:44358/api/login';

      axios.post(url, data).then(response => {
        console.log(response.data);
        this.setState({
          message: response.data
        })

        if (!this.state.message.error) {
          //cliente que esta en sesion
          console.log("guardando cliente... ", response.data)

          window.localStorage.setItem("cliente", JSON.stringify(response.data));
          // console.log(JSON.parse(window.localStorage.getItem("cliente")).nombre);

          const sessionClient = window.localStorage.getItem("cliente");
          console.log("leyendo cliente guardado ", sessionClient)

          //HACER EL CERRAR SESION PORQUE SI NO LOS DATOS SEGUIRAN SIENDO LOS MISMOS Y TAMBIEN QUE NO PUEDA VOLVER A ACCEDER A LA PAGINA DE LOGEARSE
          if (JSON.parse(window.localStorage.getItem("cliente")).id != null) {
            this.setState({
              sessionStartedClient: true
            })
          }
        }
        //console.log(this.state.sessionStarted)
      }).catch(err => {
        console.log(err);
        this.setState({
          loading: false
        })
      })

    }
    else {
      url = 'https://localhost:44358/api/loginEmpresa';
      axios.post(url, data).then(response => {
        console.log(response.data);
        this.setState({
          message: response.data
        })
        if (!this.state.message.error) {
          //cliente que esta en sesion
          console.log("guardando empresa... ", response.data)

          window.localStorage.setItem("empresa", JSON.stringify(response.data));
          //console.log(JSON.parse(prueba.getItem("empresa")).nombreFiscal);
          console.log(JSON.parse(window.localStorage.getItem("empresa")).id);

          const sessionCompany = window.localStorage.getItem("empresa");
          console.log("leyendo empresa guardado ", sessionCompany)

          if (JSON.parse(window.localStorage.getItem("empresa")).id != null) {
            this.setState({
              sessionStartedCompany: true
            })
          }
          //console.log(this.state.sessionStarted)
        }
      }).catch(err => {
        console.log(err);
        this.setState({
          loading: false
        })
      })
    }


  }

  loadingError() {
    if (this.state.loading) {
      return <p>cargando...</p>
    }
    else {
      return <p>{this.state.message}</p>
    }
  }




  clickCompany() {
    if (this.state.clickCompany === false) {
      this.setState({
        clickCompany: true
      })
      console.log("comany");
    } else {
      this.setState({
        clickCompany: false
      })
      console.log("user");
    }
  }



  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    // this.refs.main.scrollTop = 0;
  }


  render() {

    if (this.state.sessionStartedClient) {
      return <Redirect to="/listCompanies-page" />
    }

    if(this.state.sessionStartedCompany){
      return <Redirect to="/profileCompany-page"/>
    }

    return (
      <>
        <DemoNavbar />
        <main ref="main">
          <section className="section section-shaped section-lg">
            <div className="shape shape-style-1 bg-gradient-default">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            <Container className="pt-lg-md">
              <Row className="justify-content-center">
                <Col lg="5">
                  <Card className="bg-secondary shadow border-0">
                    <CardHeader className="bg-white pb-5">
                      <div className="text-muted text-center mb-3">
                        <small>Iniciar Sesión</small>
                      </div>
                      <div className="btn-wrapper text-center">
                        <Button
                          className="btn-neutral btn-icon"
                          color="default"
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          <span className="btn-inner--icon mr-1">
                            {/* <img
                              alt="..."
                              src={require("assets/img/icons/common/github.svg")}
                            /> */}
                          </span>
                          <span className="btn-inner--text">Facebook</span>
                        </Button>
                        <Button
                          className="btn-neutral btn-icon ml-1"
                          color="default"
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          <span className="btn-inner--icon mr-1">
                            <img
                              alt="..."
                              src={require("assets/img/icons/common/google.svg")}
                            />
                          </span>
                          <span className="btn-inner--text">Google</span>
                        </Button>
                      </div>
                    </CardHeader>

                    {/* Inicio de sesion */}

                    <CardBody className="px-lg-5 py-lg-5">
                      <div className="text-center text-muted mb-4">
                        <small>Datos personales</small>
                      </div>

                      <Form role="form" onSubmit={this.postData}>

                        <FormGroup className="mb-3">
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-email-83" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Email" type="email" name="email" value={this.state.email} onChange={this.dataChange} />
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-lock-circle-open" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Contraseña" type="password" name="passwordUno" autoComplete="off" value={this.state.passwordUno} onChange={this.dataChange} />
                          </InputGroup>
                        </FormGroup>

                        {/* BOTON EMPRESA */}

                        <CustomInput type="checkbox" onClick={this.clickCompany} id="exampleCustomCheckbox" label=" Empresa" />

                        <div className="custom-control custom-control-alternative custom-checkbox">
                          <input
                            className="custom-control-input"
                            id=" customCheckLogin"
                            type="checkbox"
                          />

                        </div>
                        <div className="text-center">
                          <Button
                            className="my-4"
                            color="primary"
                            type="submit"
                            value="submit"
                          >
                          Iniciar Sesión
                          </Button>
                        </div>

                      </Form>
                    </CardBody>
                  </Card>
                  <Row className="mt-3">
                    <Col xs="6">
                      <a
                        className="text-light"
                        href="/registerCompany-page"
                      >
                        <small>Registro Empresa</small>
                      </a>
                    </Col>
                    <Col className="text-right" xs="6">
                      <a
                        className="text-light"
                        href="registerClient-page"
                        // onClick={e => e.preventDefault()}
                      >
                        <small>Registro Cliente</small>
                      </a>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Container>
          </section>
        </main>
        <SimpleFooter />
      </>
    );
  }
}

export default Login;
