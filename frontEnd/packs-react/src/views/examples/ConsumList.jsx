/*!

=========================================================
* Argon Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import axios from "axios";

import BootstrapTable from 'react-bootstrap-table-next';
//import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';

import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, Container, Row, Col, Button
} from 'reactstrap';



const { SearchBar } = Search;

class ConsumList extends React.Component {
  constructor(props) {

    super(props);
    this.state = {

      prueba1: "",
      prueba: []
    }

    this.getClientes = this.getClientes.bind(this);
    this.getOfertas = this.getOfertas.bind(this);
  }
  getClientes() {
    axios.get('https://localhost:44358/api/clientes/' + JSON.parse(window.localStorage.getItem("cliente")).id)
      .then(data => data.data)
      .then(data => {
        this.setState({ prueba: data.consumos })
        console.log(this.state.prueba)
      }).catch(error => {
        error = 'Hubo un error en la base de datos'
        this.setState({ error: error })
      })
  }
  getOfertas() {
    axios.get('https://localhost:44358/api/ofertas/' + JSON.parse(window.localStorage.getItem("cliente")).consumos[0].idOferta)
      .then(data => data.data)
      .then(data => {
        this.setState({ prueba1: data })

      }).catch(error => {
        error = 'Hubo un error en la base de datos'
        this.setState({ error: error })
      })
  }

  componentDidMount() {
if(this.state.prueba1 === null){
    this.getOfertas();
    
  }
    this.getClientes();
    
  }

  render() {

    let prueba = (<h1>No hay productos</h1>)

    if (this.state.prueba != null) {
      prueba = this.state.prueba.map(consum =>


        <Col lg="3">
          <Card key={consum.id} style={{ borderRadius: "2%", boxShadow: "3px 3px 10px #666", marginTop: "30px", backgroundColor: "#222222" }}>
            <CardBody style={{ textAlign: "left", alignItems: "center", flexFlow: "column wrap" }}>
              <CardImg style={{ marginBottom: "40px" }} src={this.state.prueba1.img} alt="Card image cap" />
              <CardText style={{ marginTop: "-20px", fontSize: "12px", color: "white" }}>Cantidad:{consum.cantidad}</CardText>
              {/* <CardText style={{ marginTop: "-20px", fontSize: "12px", color: "white" }}>Cantidad:{consum.oferta}</CardText> */}
              <CardText style={{ marginTop: "-20px", fontSize: "12px", color: "white" }}>Fecha:{consum.fechaRegistro}</CardText>
              {/* <Button outline color="primary" style={{marginBottom: "-10px" ,marginTop: "-10px", width: "170px", borderRadius: "0", color: "white", borderColor: "white"}}>Button</Button>{''} */}
            </CardBody>
          </Card>
        </Col>
      )
    }
    else {
      return prueba
    }

    return (
      <>

        <Container fluid>
          <Row>
            <Col lg="12">
              <h1 style={{ marginLeft: "30%" }}>Ofertas Compradas</h1>
            </Col>
            {/* {prueba} */}
            {prueba}
          </Row>
        </Container>

      </>
    );
  }
}

export default ConsumList;
