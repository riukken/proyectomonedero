/*!

=========================================================
* Argon Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col
} from "reactstrap";

// core components
import DemoNavbar from "components/Navbars/DemoNavbar.jsx";
import SimpleFooter from "components/Footers/SimpleFooter.jsx";
import axios from "axios";

import {Map, TileLayer, Marker,} from "react-leaflet";
import { If, Else } from 'react-if';
import Leaflet from 'leaflet';
import 'leaflet/dist/leaflet.css';

Leaflet.Icon.Default.imagePath =
'../node_modules/leaflet'

delete Leaflet.Icon.Default.prototype._getIconUrl;

Leaflet.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

const MyMarker = props => {

    const initMarker = ref => {
      if (ref) {
        ref.leafletElement.openPopup()
      }
    }
  
    return <Marker ref={initMarker} {...props}/>
  }


class RegisterCompany extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nombreFiscal: '',
      nif: '',
      lat: '',
      lng: '',
      img: '',
      telefono: '',
      direccion: '',
      email: '',
      passwordUno: '',
      passwordDos: '', 
      loading: false,
      message: '',
      lat0: 41.390205,
      lng0: 2.154007,
      zoom: 13,
      resultAddress: [],
      errorResult: '',
      hasError: false,
    };
    this.postData = this.postData.bind(this);
    this.dataChange = this.dataChange.bind(this);
    this.getAddress = this.getAddress.bind(this)
    this.handleClick = this.handleClick.bind(this);
  }
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
  }


  dataChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    })

  }
//MAPA
handleClick(event){
         this.setState({lat:event.latlng.lat, lng:event.latlng.lng})   
     }


getAddress(){
        
    axios.get("https://us1.locationiq.com/v1/search.php?key=d82a4f1d87f667&q="+this.state.direccion+"&format=json")
    .then(data => data.data)
      .then(data => {
          this.setState({resultAddress: data,hasError:false})
       
      })
      .catch(error => {
           error = 'No se ha encontrado ningun resultado..'
           this.setState({errorResult: error, hasError:true})   
  })
}

getAddressByID(lat,lng){
    this.setState({
        lat: lat,
        lng: lng,
        lat0: lat,
        lng0: lng,
        zoom: 19
        
    })
    
}


  postData(event) {

    event.preventDefault()

    const nombreFiscal = this.state.nombreFiscal;
    const nif = this.state.nif;
    const lat = this.state.lat;
    const lng = this.state.lng;
    const img = this.state.img;
    const telefono = this.state.telefono;
    const direccion = this.state.direccion;
    const email = this.state.email;
    const passwordUno = this.state.passwordUno;
    const passwordDos = this.state.passwordDos;



    this.setState({
      loading: true
    })

    const data = {
      nombreFiscal,
      nif,
      lat,
      lng,
      img,
      telefono,
      direccion,
      email,
      passwordUno,
      passwordDos
    }
    console.log(data);
    axios.post('https://localhost:44358/api/empresas', data).then(response => {
      console.log(response.data);
      this.setState({
        loading: false,
        message: response.data
      })
      this.props.history.push({
        pathname: '/login-page'
      })
    }).catch(err => {
      console.log(err);
      this.setState({
        loading: false
      })
    })

  }


  loadingError() {
    if (this.state.loading) {
      return <p>cargando...</p>
    }
    else {
      return <p>{this.state.message}</p>
    }
  }

  genNum() {
    console.log("rambod");

  }

  render() {
    return (
      <>
      <DemoNavbar />
        <main ref="main">
          <section className="section section-shaped section-lg">
            <div className="shape shape-style-1 bg-gradient-default">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            <Container className="pt-lg-md">
              <Row className="justify-content-center">
                <Col lg="8">
                  <Card className="bg-secondary shadow border-0">
                    <CardHeader className="bg-white pb-5">
                      <div className="text-muted text-center mb-3">
                        <small>Registro Empresa</small>
                      </div>
                      <div className="text-center">
                        <Button
                          className="btn-neutral btn-icon mr-4"
                          color="default"
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          <span className="btn-inner--icon mr-1">
                            {/* <img
                              alt="..."
                              
                            /> */}
                          </span>
                          <span className="btn-inner--text">Facebook</span>
                        </Button>
                        <Button
                          className="btn-neutral btn-icon ml-1"
                          color="default"
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          <span className="btn-inner--icon mr-1">
                            <img
                              alt="..."
                              src={require("assets/img/icons/common/google.svg")}
                            />
                          </span>
                          <span className="btn-inner--text">Google</span>
                        </Button>
                      </div>
                    </CardHeader>

                    {/* REGISTRO EMPRESA */}

                    <CardBody className="px-lg-5 py-lg-5">
                      <div className="text-center text-muted mb-4">
                        <small>Datos empresa</small>
                      </div>
                      <Map center={{ lat:this.state.lat0, lng: this.state.lng0 }} zoom={this.state.zoom} style={{height : '400px', width:'100%'}} className='mb-3' onClick={this.handleClick}>
                        <TileLayer
                        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" 
                        />
                        {this.state.lat && this.state.lng && <MyMarker position={[this.state.lat, this.state.lng]}>
                        </MyMarker>}
                        ))}
                      </Map >
                          <If condition={this.state.hasError}>
                          <p style={{fontWeight:'bold',color:'red'}}>{this.state.errorResult}</p> 
                          <Else>
                        
                         
                        
                          {this.state.resultAddress.map(element => (
                              <div key={element.place_id} data-id={element.place_id} style={{cursor:'pointer',fontWeight:'bold',color:'black'}} onClick={this.getAddressByID.bind(this, element.lat, element.lon)}>
                              {element.display_name}<br/>
                              </div>
                              ))}
                
                          </Else>
                          </If>
                            <Button size="sm" color="primary" onClick={this.getAddress} style={{marginBottom: "5px" ,cursor:'pointer',fontWeight:'bold',color:'white'}}>Buscar dirección</Button>
                      <Form role="form" onSubmit={this.postData}>
                            <InputGroup className="input-group-alternative mb-3">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-hat-3" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input placeholder="Dirección" type="text" name="direccion" value={this.state.direccion} onChange={this.dataChange} />
                            </InputGroup>

                        <FormGroup>

                          <InputGroup className="input-group-alternative mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-hat-3" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Nombre" type="text" name="nombre" value={this.state.nombre} onChange={this.dataChange} />
                          </InputGroup>

                          <InputGroup className="input-group-alternative mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-hat-3" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Nombre Fiscal" type="text" name="nombreFiscal" value={this.state.nombreFiscal} onChange={this.dataChange} />
                          </InputGroup>

                        <FormGroup>

                          <InputGroup className="input-group-alternative mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-email-83" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Email" name="email" type="email" value={this.state.email} onChange={this.dataChange} />
                          </InputGroup>

                        </FormGroup>

                          <InputGroup className="input-group-alternative mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-hat-3" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Nif" type="text" name="nif" value={this.state.nif} onChange={this.dataChange} />
                          </InputGroup>

                          <InputGroup className="input-group-alternative mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-hat-3" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Latitud" type="text" name="lat" value={this.state.lat} onChange={this.dataChange} />
                          </InputGroup>

                          <InputGroup className="input-group-alternative mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-hat-3" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Longitud" type="text" name="lng" value={this.state.lng} onChange={this.dataChange} />
                          </InputGroup>

                          <InputGroup className="input-group-alternative mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-hat-3" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Número de telefono" type="text" name="telefono" value={this.state.telefono} onChange={this.dataChange} />
                          </InputGroup>


                        </FormGroup>


                        <FormGroup>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-lock-circle-open" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Contraseña" type="password" name="passwordUno" autoComplete="off" value={this.state.passwordUno} onChange={this.dataChange} />
                          </InputGroup>
                        </FormGroup>

                        <FormGroup>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-lock-circle-open" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Repetir contraseña" type="password" name="passwordDos" autoComplete="off" value={this.state.passwordDos} onChange={this.dataChange} />
                          </InputGroup>
                        </FormGroup>
{/* 
                        <div className="text-muted font-italic">
                          <small>
                            password strength:{" "}
                            <span className="text-success font-weight-700">
                              strong
                            </span>
                          </small>
                        </div> */}

                        <Row className="my-4">
                          <Col xs="12">
                            <div className="custom-control custom-control-alternative custom-checkbox">
                              <input
                                className="custom-control-input"
                                id="customCheckRegister"
                                type="checkbox"
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="customCheckRegister"
                              >
                                <span>
                                Acepto los terminos de privacidad {" "}
                                  <a
                                    href="#pablo"
                                    onClick={e => e.preventDefault()}
                                  >
                                    Privacy Policy
                                  </a>
                                </span>
                              </label>
                            </div>
                          </Col>
                        </Row>
                        <div className="text-center">
                          <Button
                            className="mt-4"
                            color="primary"
                            type="submit"
                            value="submit">
                            Crear cuenta
                          </Button>
                        </div>
                      </Form>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Container>
          </section>
        </main>
        <SimpleFooter />
      </>
    );
  }
}

export default RegisterCompany;
