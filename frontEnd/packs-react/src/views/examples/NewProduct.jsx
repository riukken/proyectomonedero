/*!

=========================================================
* Argon Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";


// reactstrap components
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col,
}
  from "reactstrap";
import SimpleFooter from "components/Footers/SimpleFooter.jsx";
import axios from 'axios';
import DemoNavbar from "components/Navbars/DemoNavbar.jsx";


class NewProduct extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      nombreOferta: '',
      descripcion: '',
      precio: '',
      cantidad: '',
      caducidad: '',
      img: '',
      activa: true,
      loading: false,
      message: ''

    };
    this.postData = this.postData.bind(this);
    this.dataChange = this.dataChange.bind(this);
  }
  dataChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    })

  }

  postData(event) {

    event.preventDefault()

    const descripcion = this.state.descripcion;
    const precio = this.state.precio;
    const cantidad = this.state.cantidad;
    const caducidad = this.state.caducidad;
    const img = this.state.img;
    const nombreOferta = this.state.nombreOferta;


    this.setState({
      loading: true
    })

    const data = {

      descripcion,
      precio,
      cantidad,
      caducidad,
      img,
      mensaje: '',
      activa: true,
      nombreOferta,
      idEmpresa: JSON.parse(window.localStorage.getItem("empresa")).id
    }
console.log(JSON.parse(window.localStorage.getItem("empresa")).id)

    console.log("objeto creado",data);
    axios.post('https://localhost:44358/api/ofertas', data).then(response => {
      //console.log(response.data);
      this.setState({
        message: response.data
      })

      this.props.history.push({
        pathname: '/productListCompany-page'
      })
    }).catch(err => {
      console.log(err);
      this.setState({
        loading: false
      })
      
    })

  }

  loadingError() {
    if (this.state.loading) {
      return <p>cargando...</p>
    }
    else {
      return <p>{this.state.message}</p>
    }
  }

  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
  }


  render() {


    return (
      <>
        <DemoNavbar />
        <main ref="main">
          <section className="section section-shaped section-lg">
            <div className="shape shape-style-1 shape-default alpha-4">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            <Container className="pt-lg-md">
              <Row className="justify-content-center">
                <Col lg="5">
                  <Card className="bg-secondary shadow border-0">

                    <CardBody className="px-lg-5 py-lg-5">
                      <div className="text-center text-muted mb-4">
                        <small>Crear producto</small>
                      </div>

                      <Form role="form" onSubmit={this.postData}>

                        <FormGroup className="mb-3">

                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-email-83" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input placeholder="Nombre del producto" type="text" name="nombreOferta" value={this.state.nombreOferta} onChange={this.dataChange} />
                            </InputGroup>
                          </FormGroup>

                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-email-83" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input placeholder="Precio" type="text" name="precio" value={this.state.precio} onChange={this.dataChange} />
                            </InputGroup>
                          </FormGroup>

                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-email-83" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Descripción" type="text" name="descripcion" value={this.state.descripcion} onChange={this.dataChange} onChange={this.dataChange} />
                          </InputGroup>
                        </FormGroup>

                        <FormGroup>
                          <div style={{ fontSize: "19px" }} className=" text-muted">
                            <small>Cantidad:</small>
                          </div>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                            </InputGroupAddon>
                            <Input style={{ textAlign: "center", color: "#8898aa !important", fontSize: "19px" }} type="number" name="cantidad" defaultValue="1" value={this.state.cantidad} onChange={this.dataChange} />
                          </InputGroup>
                        </FormGroup>
                        <FormGroup className="mb-3">
                          <div style={{ fontSize: "19px" }} className=" text-muted">
                            <small>Caducidad:</small>
                          </div>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-email-83" />

                              </InputGroupText>

                            </InputGroupAddon>
                            {/* <Input placeholder="Precio" type="text" name="precio" value={this.state.precio} onChange={this.dataChange}/>
                         */}

                            <Input type="date" name="caducidad"
                              value={this.state.caducidad} onChange={this.dataChange}
                              min="2019-01-01"/>
                          </InputGroup>
                        </FormGroup>

                        <FormGroup className="mb-3">
                          <div style={{ fontSize: "19px" }} className=" text-muted">
                            <small>Imagen del producto:</small>
                          </div>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-image" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Imagen" type="text" name="img" value={this.state.img} onChange={this.dataChange} />
                          </InputGroup>
                        </FormGroup>


                        <div className="custom-control custom-control-alternative custom-checkbox">
                          <input
                            className="custom-control-input"
                            id=" customCheckLogin"
                            type="checkbox"
                          />
                        </div>
                        <div className="text-center">
                          <Button
                            className="my-4"
                            color="primary"
                            type="submit"
                            value="submit"
                          >
                            crear
                          </Button>
                        </div>
                      </Form>
                    </CardBody>
                  </Card>
                  <Row className="mt-3">
                    <Col xs="6">
                      <a
                        className="text-light"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        {/* <small>Forgot password?</small> */}
                      </a>
                    </Col>
                    <Col className="text-right" xs="6">
                      <a
                        className="text-light"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        {/* <small>Create new account</small> */}
                      </a>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Container>
          </section>
        </main>
        <SimpleFooter />
      </>
    );
  }
}

export default NewProduct;