import React, { Component } from "react";
import { Redirect, Link } from 'react-router-dom';
import { NavItem, DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown, Nav, UncontrolledCollapse, Input, NavbarBrand, Navbar, InputGroupText, Form, FormGroup, InputGroup, InputGroupAddon, Card, Button, Container, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { faFacebook, faTwitter, faInstagram } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// core components
import DemoNavbar from "components/Navbars/DemoNavbar.jsx";
import axios from "axios";
import ConsumList from "views/examples/ConsumList.jsx";
import logo from "assets/img/logo/logo.png";
import Footer from "components/Footers/SimpleFooter"



class ProfileClient extends Component {


  constructor(props) {
    super(props)

    const sessionClient = window.localStorage.getItem("cliente");
    //console.log("desde constructor profile", sessionClient);

    this.state = {
      client: [],
      preview: null,
      file: null,
      modal: false,
      sessionStartedClient: sessionClient
    };
    this.toggle = this.toggle.bind(this);
    this.update = this.update.bind(this);
    this.dataChange = this.dataChange.bind(this);
  }
  //Asigno el valor del input (form) al state  
  dataChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  };

  handleSubmit(e) {
    e && e.preventDefault();


    let form_data = new FormData();
    form_data.append('file', this.state.file);
    let url = 'https://localhost:44358/api/FileUploading?idusuario=' + JSON.parse(window.localStorage.getItem("cliente")).id;
    axios.post(url, form_data, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    })
      .then(res => {
        console.log(res.data);
        this.ShowClientProfile();
      })
      .catch(err => console.log(err))
  }

  handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      }, this.handleSubmit);
    }

    reader.readAsDataURL(file)

  }

  componentDidMount() {
    if (this.state.sessionStartedClient) {
      console.log("cargando cliente")
      this.ShowClientProfile();
    }
  }

  ShowClientProfile() {
    //PONER ID DEL CLIENTE QUE HA INICIADO SESION

    axios.get('https://localhost:44358/api/clientes/' + JSON.parse(window.localStorage.getItem("cliente")).id)
      .then(data => data.data)
      .then(data => {
        console.log(data)
        this.setState({
          client: data,
          nombre: data.nombre,
          apellidos: data.apellidos,
          telefono: data.telefono,
          passwordUno: '',
          passwordDos: ''
        })

      })
  }



  update(e) {

    e.preventDefault();

    const editClient = {
      id: this.state.client.id,
      nombre: this.state.nombre,
      apellidos: this.state.apellidos,
      dni: this.state.client.dni,
      img: this.state.client.img,
      email: this.state.client.email,
      passwordUno: this.state.passwordUno,
      passwordDos: this.state.passwordDos,
      codigoPersonal: this.state.client.codigoPersonal,
      role: this.state.client.role
    }
    console.log(editClient);
    axios.put("https://localhost:44358/api/clientes/" + JSON.parse(window.localStorage.getItem("cliente")).id, editClient)
      .then(response => {
        console.log("RESPUESTA PUT", response)
        this.setState({ client: editClient });

      })
      .catch(error => {
        console.log(error);
      });

  }
  // }
  //MODAL 
  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }
  //Cerrar sesion
  singOff() {
    window.localStorage.removeItem("cliente");
  }


  render() {

    if (!this.state.sessionStartedClient) {
      return <Redirect to="/" />
    }


    var porDefecto = (<img
      alt="..."
      className="rounded-circle"
      src={require("assets/img/equipo/default.jpg")}
      height="180" width="180" />);



    if (this.state.client.img) {
      porDefecto = (<img
        alt="..."
        className="rounded-circle"
        src={"https://localhost:44358/images/" + this.state.client.img}
        height="180" width="180"
      />);
    }
    if (!this.state.client.id) {
      console.log(this.state.client);

      return <h3>Cargando..</h3>
    }

    return (
      <>



        <header className="header-global">
          <DemoNavbar />
        </header>






        <main className="profile-page" ref="main">
          <section className="section-profile-cover section-shaped my-0">
            {/* Circles background */}
            <div className="shape shape-style-1 shape-default alpha-4">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            {/* SVG separator */}
            <div className="separator separator-bottom separator-skew">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                preserveAspectRatio="none"
                version="1.1"
                viewBox="0 0 2560 100"
                x="0"
                y="0"
              >
                <polygon
                  className="fill-white"
                  points="2560 0 2560 100 0 100"
                />
              </svg>
            </div>
          </section>
          <section className="section">
            <Container>
              <Card className="card-profile shadow mt--300">
                <div className="px-4">
                  <Row className="justify-content-center">
                    <Col className="order-lg-2" lg="3" >
                      <div className="card-profile-image" >
                        {porDefecto}

                        <form onSubmit={(e) => this.handleSubmit(e)} className="text-center" >
                          <input className="fileInput" id="avatar"
                            type="file"
                            onChange={(e) => this.handleImageChange(e)} style={{ width: '0.1px', height: '0.1px', opacity: '0', overflow: 'hidden', position: 'absolute', zIndex: '-1' }} />
                          <label htmlFor="avatar" style={{ cursor: 'pointer', marginTop: '65%', fontWeight: "bold" }}>Cambiar imagen</label>

                        </form>

                      </div>
                    </Col>
                    <Col
                      className="order-lg-3 text-lg-right align-self-lg-center"
                      lg="4"
                    >
                    </Col>
                    <Col className="order-lg-1" lg="4">
                      <div className="card-profile-stats d-flex justify-content-center">
                        <div>
                          <span style={{ cursor: 'pointer' }} className="description" onClick={this.toggle}>Configuración</span>
                        </div>
                      </div>
                    </Col>
                  </Row>
                  <div className="text-center mt-3 mb-5">
                    <div className="d-flex justify-content-center">
                      <FontAwesomeIcon icon={faFacebook} className="mb-3" size="lg" />
                      <FontAwesomeIcon icon={faTwitter} className="ml-2 mb-3" size="lg" />
                      <FontAwesomeIcon icon={faInstagram} className="ml-2 mb-3" size="lg" />
                    </div>
                    <h3>
                      {this.state.client.nombre} {this.state.client.apellidos}
                    </h3>
                    <div className="h6 mt-4">
                      <i className="ni business_briefcase-24 mr-2" />
                      DNI: {this.state.client.dni}
                    </div>
                    <div className="h6 mt-4">
                      <i className="ni business_briefcase-24 mr-2" />
                      Codigo Personal: 934857673723
                    </div>
                    <div className="h6 mt-4">
                      <i className="ni business_briefcase-24 mr-2" />
                      Email: {this.state.client.email}
                    </div>
                  </div>
                </div>



                <Modal isOpen={this.state.modal} toggle={this.toggle} modalTransition={{ timeout: 700 }} backdropTransition={{ timeout: 1300 }}
                  className="prueba">
                  <ModalHeader >Editar Datos del perfil</ModalHeader>
                  <Form role="form" onSubmit={this.update}>



                    <ModalBody>

                      <FormGroup className="mb-3">
                        <InputGroup className="input-group-alternative">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="ni ni-circle-08" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input type="text" name="nombre" value={this.state.nombre} onChange={this.dataChange} />
                        </InputGroup>
                      </FormGroup>

                      <FormGroup className="mb-3">
                        <InputGroup className="input-group-alternative">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="ni ni-circle-08" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input type="text" name="apellidos" value={this.state.apellidos} onChange={this.dataChange} />
                        </InputGroup>
                      </FormGroup>


                      <FormGroup>
                        <InputGroup className="input-group-alternative">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="ni ni-lock-circle-open" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input placeholder="Nueva Contraseña" type="password" name="passwordUno" autoComplete="off" value={this.state.passwordUno} onChange={this.dataChange} />
                        </InputGroup>
                      </FormGroup>

                      <FormGroup>
                        <InputGroup className="input-group-alternative">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="ni ni-lock-circle-open" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input placeholder="Repita la nueva Contraseña" type="password" name="passwordDos" autoComplete="off" value={this.state.passwordDos} onChange={this.dataChange} />
                        </InputGroup>
                      </FormGroup>


                    </ModalBody>


                    <ModalFooter>
                      <Button color="primary" type="submit" value="submit" onClick={this.toggle}>Guardar cambios</Button>
                      <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                  </Form>

                </Modal>
              </Card>
              <Row>
                <ConsumList />
              </Row>
            </Container>
          </section>
          <Footer />
        </main>
      </>
    );
  }
}

export default ProfileClient;