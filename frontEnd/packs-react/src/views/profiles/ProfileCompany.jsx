import React, { Component } from "react";
import { Redirect, Link } from 'react-router-dom';
import { Input, InputGroupText, Form, FormGroup, InputGroup, InputGroupAddon, Card, Button, Container, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { faFacebook, faTwitter, faInstagram } from "@fortawesome/free-brands-svg-icons"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// core components
import DemoNavbar from "components/Navbars/DemoNavbar.jsx";
import axios from "axios";
import Leaflet from 'leaflet';
import 'leaflet/dist/leaflet.css';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';
import Footer from 'components/Footers/SimpleFooter';



Leaflet.Icon.Default.imagePath =
  '../node_modules/leaflet'

delete Leaflet.Icon.Default.prototype._getIconUrl;

Leaflet.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

class ProfileCompany extends Component {
  constructor(props) {
    super(props)
    const sessionCompany = window.localStorage.getItem("empresa");
    this.state = {
      company: [],
      lat: 41.390205,
      lng: 2.154007,
      zoom: 13,
      preview: null,
      file: null,
      modal: false,
      sessionStartedCompany: sessionCompany
      //ofertas: []
    };

    this.toggle = this.toggle.bind(this);
    this.update = this.update.bind(this);
    this.dataChange = this.dataChange.bind(this);
  }

  //Asigno el valor del input (form) al state  
  dataChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  };


  handleSubmit(e) {

    e && e.preventDefault();

    console.log(this.state);
    let form_data = new FormData();
    form_data.append('file', this.state.file);
    let url = 'https://localhost:44358/api/FileUploadingCompany?idcompany=' + JSON.parse(window.localStorage.getItem("empresa")).id;
    axios.post(url, form_data, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    })
      .then(res => {
        console.log(res.data);
        this.ShowCompanyProfile();
      })
      .catch(err => console.log(err))
  }

  handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      }, this.handleSubmit);
    }

    reader.readAsDataURL(file)

  }

  componentDidMount() {
    if (this.state.sessionStartedCompany) {
      this.ShowCompanyProfile();
    }
  }

  ShowCompanyProfile() {
    //PONER ID DEL CLIENTE QUE HA INICIADO SESION
    axios.get('https://localhost:44358/api/empresas/' + JSON.parse(window.localStorage.getItem("empresa")).id)
      .then(data => data.data)
      .then(data => {
        console.log(data)
        this.setState({
          company: data,
          nombreFiscal: data.nombreFiscal,
          // nif: '',
          //lat: '',
          //lng: '',
          telefono: data.telefono,
          //direccion: '',
          passwordUno: '',
          passwordDos: '',
        })
      })
  }


  update(e) {
    e.preventDefault();
    const editCompany = {
      id: this.state.company.id,
      nombreFiscal: this.state.nombreFiscal,
      nif: this.state.company.nif,
      lat: this.state.company.lat,
      lng: this.state.company.lng,
      img: this.state.company.img,
      telefono: this.state.telefono,
      direccion: this.state.company.direccion,
      email: this.state.company.email,
      passwordUno: this.state.passwordUno,
      passwordDos: this.state.passwordDos,
      role: this.state.company.role,
      ofertas: this.state.company.ofertas
    }
    console.log(editCompany);
    axios.put("https://localhost:44358/api/empresas/" + JSON.parse(window.localStorage.getItem("empresa")).id, editCompany)
      .then(response => {
        console.log("RESPUESTA PUT", response)
        this.setState({ company: editCompany });

      })
      .catch(error => {
        console.log(error);
      });
  }


  //MODAL 
  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  // //Cerrar sesion
  // singOff() {
  //   window.localStorage.removeItem("empresa");
  // }


  render() {

    if (!this.state.sessionStartedCompany) {
      return <Redirect to="/" />
    }



    const position = [this.state.lat, this.state.lng]

    var imgDefault = (<img
      alt="..."
      className="rounded-circle"
      src={require("assets/img/equipo/default.jpg")}
      height="180" width="180" />);

    if (this.state.company.img) {
      imgDefault = (<img
        alt="..."
        className="rounded-circle"
        src={"https://localhost:44358/images/" + this.state.company.img}
        height="180" width="180"
      />);
    }

    if (!this.state.company.id) {
      return <h3>Cargando..</h3>
    }
    return (
      <>
        <DemoNavbar />
        <main className="profile-page" ref="main">
          <section className="section-profile-cover section-shaped my-0">
            {/* Circles background */}
            <div className="shape shape-style-1 shape-default alpha-4">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            {/* SVG separator */}
            <div className="separator separator-bottom separator-skew">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                preserveAspectRatio="none"
                version="1.1"
                viewBox="0 0 2560 100"
                x="0"
                y="0"
              >
                <polygon
                  className="fill-white"
                  points="2560 0 2560 100 0 100"
                />
              </svg>
            </div>
          </section>

          <section className="section">
            <Container>
              <Card className="card-profile shadow mt--300">
                <div className="px-4">
                  <Row className="justify-content-center">
                    <Col className="order-lg-2" lg="3" >
                      <div className="card-profile-image" >
                        {imgDefault}

                        <form onSubmit={(e) => this.handleSubmit(e)} className="text-center" >
                          <input className="fileInput" id="avatar"
                            type="file"
                            onChange={(e) => this.handleImageChange(e)} style={{ width: '0.1px', height: '0.1px', opacity: '0', overflow: 'hidden', position: 'absolute', zIndex: '-1' }} />
                          <label htmlFor="avatar" style={{ cursor: 'pointer', marginTop: "65%", fontWeight: "bold" }}>Cambiar imagen
                          </label>
                        </form>
                      </div>

                    </Col>

                    <Col className="order-lg-3 text-lg-right align-self-lg-center" lg="4"></Col>


                    <Col className="order-lg-1" lg="4">
                      <div className="card-profile-stats d-flex justify-content-center">
                        {/* <div> */}
                        {/* CAMBIAR Y PONERLO BIEN */}
                        {/* <span style={{ cursor: 'pointer' }} className="description"><Link to="/productList-page">Ofertas creadas</Link></span>
                        </div> */}
                        <div>
                          <span style={{ cursor: 'pointer' }} className="description" onClick={this.toggle}>Configuración</span>
                        </div>
                      </div>
                    </Col>


                  </Row>
                  <div className="text-center  mt-3 mb-5">

                    <h1>
                      {this.state.company.nombreFiscal}
                    </h1>


                    <div className="d-flex justify-content-center">
                      <FontAwesomeIcon icon={faFacebook} className="mb-3" size="lg" />
                      <FontAwesomeIcon icon={faTwitter} className="ml-2 mb-3" size="lg" />
                      <FontAwesomeIcon icon={faInstagram} className="ml-2 mb-3" size="lg" />
                    </div>


                    <div className="h6 mt-4">
                      <h5>Dirección: {this.state.company.direccion}</h5>
                    </div>
                    <div className="h6 mt-4">
                      <h5>Teléfono: {this.state.company.telefono}</h5>
                    </div>
                    <div className="h6 mt-4">
                      <h5>Email: {this.state.company.email}</h5>
                    </div>

                    <div className="h6 mt-4">


                      <Modal isOpen={this.state.modal} toggle={this.toggle} modalTransition={{ timeout: 700 }} backdropTransition={{ timeout: 1300 }}
                        className="prueba">
                        <ModalHeader >Editar Datos del perfil</ModalHeader>
                        <Form role="form" onSubmit={this.update}>



                          <ModalBody>

                            <FormGroup className="mb-3">
                              <InputGroup className="input-group-alternative">
                                <InputGroupAddon addonType="prepend">
                                  <InputGroupText>
                                    <i className="ni ni-email-83" />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" name="nombreFiscal" value={this.state.nombreFiscal} onChange={this.dataChange} />
                              </InputGroup>
                            </FormGroup>


                            <FormGroup>
                              <InputGroup className="input-group-alternative">
                                <InputGroupAddon addonType="prepend">
                                  <InputGroupText>
                                    <i className="ni ni-tablet-button" />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Input placeholder="Telefono" type="tel" name="telefono" autoComplete="off" value={this.state.telefono} onChange={this.dataChange} />
                              </InputGroup>
                            </FormGroup>

                            <FormGroup>
                              <InputGroup className="input-group-alternative">
                                <InputGroupAddon addonType="prepend">
                                  <InputGroupText>
                                    <i className="ni ni-lock-circle-open" />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Input placeholder="Nueva Contraseña" type="password" name="passwordUno" autoComplete="off" value={this.state.passwordUno} onChange={this.dataChange} />
                              </InputGroup>
                            </FormGroup>

                            <FormGroup>
                              <InputGroup className="input-group-alternative">
                                <InputGroupAddon addonType="prepend">
                                  <InputGroupText>
                                    <i className="ni ni-lock-circle-open" />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Input placeholder="Repita la nueva Contraseña" type="password" name="passwordDos" autoComplete="off" value={this.state.passwordDos} onChange={this.dataChange} />
                              </InputGroup>
                            </FormGroup>

                            {/* ESTO LO TIENE QUE HACER EL JOAN */}
                            {/* <FormGroup>
                              <InputGroup className="input-group-alternative">
                                <InputGroupAddon addonType="prepend">
                                  <InputGroupText>
                                    <i className="ni ni-lock-circle-open" />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Input placeholder="Direccion" type="text" name="passwordUno" autoComplete="off" value={this.state.company.direccion} onChange={this.dataChange} />
                              </InputGroup>
                            </FormGroup>
                             */}

                          </ModalBody>


                          <ModalFooter>
                            <Button color="primary" type="submit" value="submit" onClick={this.toggle}>Guardar cambios</Button>
                            <Button color="secondary" onClick={this.toggle}>Cancelar</Button>
                          </ModalFooter>
                        </Form>
                      </Modal>

                    </div>

                    <Map center={position} zoom={this.state.zoom} style={{ height: '400px', width: '100%' }} className='mb-3'>
                      <TileLayer
                        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                      />

                      <Marker key={this.state.company.id} position={[this.state.company.lat, this.state.company.lng]}>
                        <Popup>
                          <p><span style={{ fontWeight: 'bold' }}>Empresa:</span> {this.state.company.nombreFiscal}</p>
                          <p><span style={{ fontWeight: 'bold' }}>Direccion:</span> {this.state.company.direccion}</p>
                          <button className="btn btn-primary">Ver ofertas</button>
                        </Popup>
                      </Marker>

                    </Map >
                  </div>
                </div>
              </Card>
            </Container>
          </section>
            <Footer />
        </main>
      </>
    );
  }
}

export default ProfileCompany;