import React from "react";
import axios from 'axios';
//import { Table } from 'reactstrap';
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { Map, Marker, Popup, TileLayer } from 'react-leaflet'
import Leaflet from 'leaflet';
import 'leaflet/dist/leaflet.css';
import DemoNavbar from "components/Navbars/DemoNavbar";
import BootstrapTable from 'react-bootstrap-table-next';
//import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import Footer from 'components/Footers/SimpleFooter';


const { SearchBar } = Search;

//import SimpleFooter from "components/Footers/SimpleFooter";

//import DemoNavbar from "components/Navbars/DemoNavbar.jsx";

Leaflet.Icon.Default.imagePath =
  '../node_modules/leaflet'

delete Leaflet.Icon.Default.prototype._getIconUrl;

Leaflet.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

class ListCompanies extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      companies: [],
      lat: 41.390205,
      lng: 2.154007,
      zoom: 13,
      error: '',
      columns: [
        {
          dataField: 'nombreFiscal',
          text: 'nombreFiscal',
          //filter: textFilter({placeholder:'Buscar por nombre'}),
          headerStyle: {
            backgroundColor: '#5e72e4',
            color: '#fff'
          },

        }, {
          dataField: 'email',
          text: 'Email',
          headerStyle: {
            backgroundColor: '#5e72e4',
            color: '#fff'
          }
        },

        {
          dataField: 'direccion',
          text: 'Direccion',
          headerStyle: {
            backgroundColor: '#5e72e4',
            color: '#fff'
          }
        },

        {
          dataField: 'Ofertas',
          text: 'Ver ofertas',
          formatter: (rowContent, row) => {
            return (
              <button className="btn btn-primary" onClick={this.ViewOffers.bind(this, row)}>Ver ofertas</button>
            )
          },
          headerStyle: {
            backgroundColor: '#5e72e4',
            color: '#fff'
          }
        }
      ]
    }

  }
  componentDidMount() {
    this.showCompanies()
  }
  /*
  FilterbyName() {
      let input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementsByClassName("table");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
  */
  ViewOffers(element) {
    this.props.history.push({
      pathname: '/ProductList'
    })
    localStorage.ofertas=JSON.stringify(element.ofertas)
    //console.log(element)
   }
    showCompanies() {
        axios.get('https://localhost:44358/api/empresas')
        .then(data => data.data)
          .then(data => {
            //console.log(data)
              this.setState({companies: data})
              
          }).catch(error => {
                error = 'Hubo un error en la base de datos'
                this.setState({error: error})
          })
    }
    
    render() {
      
        if (!this.state.companies.length){
            return <h3>Cargando datos...</h3>
        }
        const position = [this.state.lat, this.state.lng]
        return (
<>
            <div  style={{marginBottom:"5%"}}>
              <DemoNavbar />
              <section className="section-profile-cover section-shaped my-0">
            {/* Circles background */}
            <div className="shape shape-style-1 shape-default alpha-4">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            </section>
              <h1 className="text-center" style={{marginTop: '-28%', marginBottom:'2%', color: "white"}}>Empresas</h1>
                <Map center={position} zoom={this.state.zoom} style={{height : '600px', width:'100%'}} className='mb-3'>
                    <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {this.state.companies.map(element => (
                    <Marker  key={element.id} position={[element.lat, element.lng]}>
                    <Popup>
                        <p><span style={{fontWeight:'bold'}}>Nombre Empresa:</span> {element.nombreFiscal}</p>
                        <p><span style={{fontWeight:'bold'}}>Direccion:</span> {element.direccion}</p>
                        <button className="btn btn-primary">Ver ofertas</button>
                    </Popup>
                    </Marker>
                    ))}
                </Map >
            
            
           {/* <Table className="mt-3" id="myTable">  git reset --hard
                <thead className="btn-primary">  
                    <tr>  
                        <th>Nombre</th>  
                        <th>Nombre Fiscal</th>  
                        <th>Email</th>  
                        <th>Telefono</th>  
                        <th>Direccion</th>    
                    </tr>  
                </thead>  
                <tbody>  
                    {/*this.state.companies.map(company => (  
                    <tr key={company.id}>  
                        <td>{company.nombre}</td>
                        <td>{company.nombreFiscal}</td>   
                        <td>{company.email}</td> 
                        <td>{company.telefono}</td>
                        <td>{company.direccion}</td>
                    </tr>  
                    ))*/}
            {/*</tbody>*/}
            {/*</Table>*/}
            <ToolkitProvider
           
              keyField={"id"}
              data={this.state.companies}
              columns={this.state.columns}
              search
            >
              {
                props => (
                  <div>
                    <FontAwesomeIcon icon={faSearch} className="ml-2 mr-2" /><SearchBar {...props.searchProps} placeholder="Buscar por nombre" />
                    <div className="table-responsive">
                      <BootstrapTable
                        {...props.baseProps}
                        pagination={paginationFactory()}
                      />


                    </div>

                  </div>
                )
              }
            </ToolkitProvider>
            {/*<SimpleFooter/>*/}
          </div>
<Footer/>
</>
    );
  }
}

export default ListCompanies;
