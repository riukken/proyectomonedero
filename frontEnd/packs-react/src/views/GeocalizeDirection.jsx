import React, { Component } from "react";
import {Map, TileLayer, Marker,} from "react-leaflet";
import { If, Else } from 'react-if'
import Leaflet from 'leaflet';
import 'leaflet/dist/leaflet.css';
import axios from 'axios';

Leaflet.Icon.Default.imagePath =
'../node_modules/leaflet'

delete Leaflet.Icon.Default.prototype._getIconUrl;

Leaflet.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

const MyMarker = props => {

    const initMarker = ref => {
      if (ref) {
        ref.leafletElement.openPopup()
      }
    }
  
    return <Marker ref={initMarker} {...props}/>
  }

class Geocalize extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            lat: null,
            lng: null,
            zoom: 13,
            address: '',
            pepe: '',
            resultAddress: [],
            errorResult: '',
            hasError: false,
            lat0: 41.390205,
            lng0: 2.154007,
        }
        this.dataChange = this.handleChange.bind(this)
        this.getAddress = this.getAddress.bind(this)
        this.handleClick = this.handleClick.bind(this);

    }

    handleChange = (e) => {
        this.setState({[e.target.name]:e.target.value})
     }

    handleClick(e){
        this.setState({lat:e.latlng.lat, lng:e.latlng.lng})   
    }

    getAddress(){
        
        axios.get("https://us1.locationiq.com/v1/search.php?key=d82a4f1d87f667&q="+this.state.address+"&format=json")
        .then(data => data.data)
          .then(data => {
              this.setState({resultAddress: data,hasError:false})
           
          })
          .catch(error => {
               error = 'No se ha encontrado ningun resultado..'
               this.setState({errorResult: error, hasError:true})   
      })
    }

    getAddressByID(lat,lng){
        this.setState({
            lat: lat,
            lng: lng,
            lat0: lat,
            lng0: lng,
            zoom: 19
            
        })
        
    }
    render() { 
       
        return (  
            <div>
                <Map center={{ lat:this.state.lat0, lng: this.state.lng0 }} zoom={this.state.zoom} style={{height : '600px', width:'100%'}} className='mb-3' onClick={this.handleClick}>
                    <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" 
                    />
                     {this.state.lat && this.state.lng && <MyMarker position={[this.state.lat, this.state.lng]}>
                     </MyMarker>}
                    ))}
                </Map >

                <If condition={this.state.lat == null && this.state.lng == null}>
                Lat: <input type="text" value="lat"/>
                Lng: <input type="text" value="lng"/>
                <Else>
                Lat: <input type="text" value={this.state.lat} onChange={this.getAddress}/>
                Lng: <input type="text" value={this.state.lng} onChange={this.getAddress}/>    
                </Else>
                </If>

                <div id="search">
                <input type="text" name="address" id="address"  value={ this.state.address }
                onChange={ this.handleChange } size="58" />
                <br/>
                
                <If condition={this.state.hasError}>
                <p style={{fontWeight:'bold',color:'red'}}>{this.state.errorResult}</p> 
                <Else>
               
                {/*this.state.hasError=false*/}
               
                {this.state.resultAddress.map(element => (
                    <div key={element.place_id} data-id={element.place_id} style={{cursor:'pointer'}} onClick={this.getAddressByID.bind(this, element.lat, element.lon)}>
                    {element.display_name}<br/>
                    </div>
                    ))}
                
                </Else>
                </If>
                <button type="button" className='btn btn-primary mt-2' onClick={this.getAddress}>Search</button>
       
                </div>
               
            </div>


        )
    }
}
 
export default Geocalize;