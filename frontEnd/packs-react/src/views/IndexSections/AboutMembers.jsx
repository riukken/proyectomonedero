import React from "react";

import { Container, Row, Col } from "reactstrap";
import DemoNavbar from "components/Navbars/DemoNavbar";
import Members from "./Members.jsx"
import SimpleFooter from "components/Footers/SimpleFooter.jsx";


class About extends React.Component {
  render() {
    return (
      <>
        <DemoNavbar />
        <section className="section section-shaped">
          <div className="shape shape-style-1 shape-default">
            <span />
            <span />
            <span />
            <span />
            <span />
            <span />
          </div>
          <Container className="py-md">
            <Row className="justify-content-between align-items-center">
              <Col className="mb-5 mb-lg-0" lg="10">
                <h1 className="text-white font-weight-light">
                  Sobre Nosotros
                </h1>
                <p className="lead text-white mt-4">
                  Somos 5 estudiantes de programación web y administración de servidores de BBDD,
                  especializados en ReactJS, C# y SQL server.
                  Nuestro objetivo es poder facilitar las transacciones del día a día, rápidas y fáciles,
                  pudiendo a la vez fidelizar a más clientes de los pequeños negocios como bares,
                  panaderías, peluquerías, etc.
                </p>
              </Col>
            </Row>
          </Container>
          {/* SVG separator */}
          <div className="separator separator-bottom separator-skew">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              preserveAspectRatio="none"
              version="1.1"
              viewBox="0 0 2560 100"
              x="0"
              y="0"
            >
              <polygon className="fill-white" points="2560 0 2560 100 0 100" />
            </svg>
          </div>
        </section>
        <Members />
        <SimpleFooter />
      </>
    );
  }
}

export default About;