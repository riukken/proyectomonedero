import React from "react";

import { Row, Col, Modal, Button, Container, ModalHeader } from "reactstrap";
import Hector from "../../../src/assets/img/us/hector.jpg";
import Victor1 from "../../../src/assets/img/us/victor1.jpg";
import Facundo from "../../../src/assets/img/us/facundo.jpg";
import Joan from "../../../src/assets/img/us/joan.jpg";
import Victor from "../../../src/assets/img/us/victor.jpg";


class Members extends React.Component {

    state = {};

    fotoModal1 = state => {
        this.setState({
            [state]: !this.state[state]
        });
    };

    fotoModal2 = state => {
        this.setState({
            [state]: !this.state[state]
        });
    };

    fotoModal3 = state => {
        this.setState({
            [state]: !this.state[state]
        });
    };

    fotoModal4 = state => {
        this.setState({
            [state]: !this.state[state]
        });
    };

    fotoModal5 = state => {
        this.setState({
            [state]: !this.state[state]
        });
    };

    render() {
        return (
            <>

                <Container>
                    <h1 className="mt-lg mb-5">
                        <span>Miembros del Equipo</span>
                    </h1>
                    <Row>
                        <Col xs="4">
                            <img
                                alt="Victor Luque Buxadé"
                                className="img-fluid rounded-circle shadow-lg"
                               src={Victor1}
                                style={{ width: "150px" }}
                                onClick={() => this.fotoModal1("imageModal1")} />
                            <small className="d-block text-uppercase font-weight-bold mb-4">
                                Victor Luque Buxadé
                            </small>
                            <Modal
                                className="modal-dialog-centered"
                                size="md"
                                isOpen={this.state.imageModal1}
                                toggle={() => this.fotoModal1("imageModal1")}>
                                <ModalHeader>
                                    <Button
                                        onClick={() => this.fotoModal1("imageModal1")}
                                        close>
                                    </Button>
                                    <Row className="col-md-12">
                                        <Col className="col-md-4">
                                            <img
                                                alt="Victor Luque Buxadé"
                                                className="img-fluid rounded-circle shadow-lg"
                                                src={Victor1}
                                                style={{ width: "150px" }} />
                                        </Col>
                                        <Col className="col-md-8">
                                            <h3>Victor Luque Buixadé</h3>
                                            <h6>Programador FullStack Junior Developer</h6>
                                        </Col>
                                    </Row>
                                </ModalHeader>
                                <br></br>
                                <Col xs="12">
                                    <dl style={{textAlign: "center"}}>
                                        
                                        <i class="fas fa-phone"></i>
                                        <dd>  670 74 88 72</dd>
                                        <i class="fas fa-envelope"/> 
                                        <dd> <a href="mailto:victorluquebuxade@gmail.com">victorluquebuxade@gmail.com</a></dd>
                                        <dd>
                                            <i class="fas fa-graduation-cap"><br></br>
                                            2019 Habilidades y competencias tecnológicas en .NET/C#<br></br>
                                            2019 JavaScript FullStack Junior developer 
                                            </i>
                                        </dd>
                                    </dl>
                                </Col>
                            </Modal>
                        </Col>
                        <Col xs="4">
                            <img
                                alt="Victor Luque Santos"
                                className="img-fluid rounded-circle shadow-lg"
                                src={Victor}
                                style={{ width: "150px" }}
                                onClick={() => this.fotoModal2("imageModal2")} />
                            <small className="d-block text-uppercase font-weight-bold mb-4">
                                Victor Luque Santos
                            </small>
                            <Modal
                                className="modal-dialog-centered"
                                size="md"
                                isOpen={this.state.imageModal2}
                                toggle={() => this.fotoModal2("imageModal2")}>
                                <ModalHeader>
                                    <Button
                                        onClick={() => this.fotoModal2("imageModal2")}
                                        close>
                                    </Button>
                                    <Row className="col-md-12">
                                        <Col className="col-md-4">
                                            <img
                                                alt="Victor Luque Santos"
                                                className="img-fluid rounded-circle shadow-lg"
                                                src={Victor}
                                                style={{ width: "150px" }} />
                                        </Col>
                                        <Col className="col-md-8">
                                            <h3>Victor Luque Santos</h3>
                                            <h6>Programador FullStack Junior Developer</h6>
                                        </Col>
                                    </Row>
                                </ModalHeader>
                                <br></br>
                                <Col xs="12">
                                    <dl style={{textAlign: "center"}}>
                                    <i class="fas fa-phone"/>
                                        <dd>  637 28 41 87</dd>
                                        <i class="fas fa-envelope" />
                                        <dd><a href="mailto:victorlusa96@gmail.com">victorlusa96@gmail.com</a></dd>
                                        <dd>
                                            <i class="fas fa-graduation-cap"><br></br>  
                                            2019 Habilidades y competencias tecnológicas en .NET/C#<br></br>
                                            2019 Desarrollo de aplicaciones Web CFGS, DAW<br></br>

                                            </i>
                                        </dd>
                                    </dl>
                                </Col>
                            </Modal>
                        </Col>
                        <Col xs="4">
                            <img
                                alt="Joan Miquel Ortega"
                                className="img-fluid rounded-circle shadow-lg"
                                src={Joan}
                                style={{ width: "150px" }}
                                onClick={() => this.fotoModal3("imageModal3")} />
                            <small className="d-block text-uppercase font-weight-bold mb-4">
                                Joan Miquel Ortega</small>
                            <Modal
                                className="modal-dialog-centered"
                                size="md"
                                isOpen={this.state.imageModal3}
                                toggle={() => this.fotoModal3("imageModal3")}
                            >
                                <ModalHeader>
                                    <Button
                                        onClick={() => this.fotoModal3("imageModal3")}
                                        close>
                                    </Button>
                                    <Row className="col-md-12">
                                        <Col className="col-md-4">
                                            <img
                                                alt="Joan Miquel Ortega"
                                                className="img-fluid rounded-circle shadow-lg"
                                                src={Joan}
                                                style={{ width: "150px" }} />
                                        </Col>
                                        <Col className="col-md-8">
                                            <h3>Joan Miquel Ortega</h3>
                                            <h6>Programador FullStack Junior Developer</h6>
                                        </Col>
                                    </Row>
                                </ModalHeader>
                                <br></br>
                                <Col xs="12">
                                    <dl style={{textAlign: "center"}}>
                                    <i class="fas fa-phone"/>
                                        <dd>693 30 03 22</dd>
                                        <i class="fas fa-envelope"/>
                                        <dd><a href="mailto:joanmiquelortega@gmail.com">  joanmiquelortega@gmail.com</a></dd>
                                        <dd>
                                            <i class="fas fa-graduation-cap"><br></br>
                                            2019 Habilidades y competencias tecnológicas en .NET/C#<br></br>
                                            2019 JavaScript FullStack Junior developer<br></br>
                                            2018 Técnico Superior en Desarrollo de Aplicaciones Web<br></br>
                                            2015 Técnico en sistemas microinformáticos y redes
                                            </i>
                                        </dd>
                                    </dl>
                                </Col>
                            </Modal>
                        </Col>
                        <Col xs="4">
                            <img
                                alt="Hector Quintanilla García"
                                className="img-fluid rounded-circle shadow-lg"
                               src={Hector}
                                style={{ width: "150px" }}
                                onClick={() => this.fotoModal4("imageModal4")} />

                            <small className="d-block text-uppercase font-weight-bold mb-4">
                                Hector Quintanilla García</small>
                            <Modal
                                className="modal-dialog-centered"
                                size="md"
                                isOpen={this.state.imageModal4}
                                toggle={() => this.fotoModal4("imageModal4")}>
                                <ModalHeader>
                                    <Button
                                        onClick={() => this.fotoModal4("imageModal4")}
                                        close>
                                    </Button>
                                    <Row className="col-md-12">
                                        <Col className="col-md-4">
                                            <img
                                                alt="Hector Quintanilla García"
                                                className="img-fluid rounded-circle shadow-lg"
                                               src={Hector}
                                                style={{ width: "150px" }} />
                                        </Col>
                                        <Col className="col-md-8">
                                            <h3>Hector Quintilla García</h3>
                                            <h6>Programador FullStack Junior Developer</h6>
                                        </Col>
                                    </Row>
                                </ModalHeader>
                                <br></br>
                                <Col xs="12">
                                    <dl style={{textAlign: "center"}}>
                                    <i class="fas fa-phone"/>
                                        <dd>  677 64 00 51</dd>
                                        <i class="fas fa-envelope"/> 
                                        <dd><a href="mailto:orlandohector41@gmail.com">orlandohector41@gmail.com</a></dd>
                                        <dd><i class="fas fa-graduation-cap"><br></br>
                                            2019 Habilidades y competencias tecnológicas en .NET/C#<br></br>
                                            2019 JavaScript FullStack Junior developer<br></br>
                                            </i>
                                        </dd>
                                    </dl>
                                </Col>
                            </Modal>
                        </Col>
                        <Col xs="4">
                            <img
                                alt="Facundo Martín García"
                                className="img-fluid rounded-circle shadow-lg"
                               src={Facundo}
                                style={{ width: "150px" }}
                                onClick={() => this.fotoModal5("imageModal5")} />
                            <small className="d-block text-uppercase font-weight-bold mb-4">
                                Facundo Martín García
                            </small>
                            <Modal
                                className="modal-dialog-centered"
                                size="md"
                                isOpen={this.state.imageModal5}
                                toggle={() => this.fotoModal5("imageModal5")}>
                                <ModalHeader>
                                    <Button
                                        onClick={() => this.fotoModal5("imageModal5")}
                                        close>
                                    </Button>
                                    <Row className="col-md-12">
                                        <Col className="col-md-4">
                                            <img
                                                alt="Facundo Martín García"
                                                className="img-fluid rounded-circle shadow-lg"
                                               src={Facundo}
                                                style={{ width: "150px" }} />
                                        </Col>
                                        <Col className="col-md-8">
                                            <h3>Facundo Martín García Almirón</h3>
                                            <h6>Programador FullStack Junior Developer</h6>
                                        </Col>
                                    </Row>
                                </ModalHeader>
                                <br></br>
                                <Col xs="12">
                                    <dl style={{textAlign: "center"}}>
                                    <i class="fas fa-phone"/>
                                        <dd> 654 34 12 47</dd>
                                        <i class="fas fa-envelope"/>
                                        <dd><a href="mailto:facgar24@gmail.com">  facgar24@gmail.com</a></dd>
                                        <dd><i class="fas fa-graduation-cap"><br></br>
                                            2019 Habilidades y competencias tecnológicas en .NET/C#<br></br>
                                            2019 JavaScript FullStack Junior developer<br></br>
                                            2017 Técnico en Seguridad Informática<br></br>
                                            2016 Técnico en Redes
                                            </i>
                                        </dd>
                                    </dl>
                                </Col>
                            </Modal>
                        </Col>
                    </Row>
                </Container >
            </>
        )
    }
}

export default Members;