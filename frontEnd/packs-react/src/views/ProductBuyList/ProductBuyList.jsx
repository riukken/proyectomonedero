import React, { Component } from 'react';
import {
    Container, Row
} from 'reactstrap';
import "./ProductBuyList.css";
import DragList from "./DragList.jsx";
import DemoNavbar from "components/Navbars/DemoNavbar.jsx";
import Footer from 'components/Footers/SimpleFooter';


// const demoProducts = [

//     {
//         id: "1",
//         img: "https://1.bp.blogspot.com/-Y8H-0DWygcI/Uulo6hMuUNI/AAAAAAAALYA/xRbwoLi0qGc/s1600/unsplash_52d5bbef8a613_1.JPG",
//         title: "Macarons", description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, temporibus.", price: 6, expireDate: "10/12/2020", maxQuantity: 10
//     },
//     {
//         id: "1",
//         img: "https://1.bp.blogspot.com/-Y8H-0DWygcI/Uulo6hMuUNI/AAAAAAAALYA/xRbwoLi0qGc/s1600/unsplash_52d5bbef8a613_1.JPG",
//         title: "Macarons", description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, temporibus.", price: 6, expireDate: "10/12/2020", maxQuantity: 10
//     }
// ]


class ProductBuyList extends Component {
    constructor(props) {
        super(props);
        // this.state = {
        //     dataProducts: []
        // };
        //const ofertas = JSON.parse(localStorage.ofertas);
//onsole.log(ofertas)
        //this.setState({dataProducts:ofertas})
        //console.log(this.state.dataProducts)
    };


    render() {
        
        const ofertas = JSON.parse(localStorage.ofertas);
        console.log(ofertas)
     
        return (
            <>
            <DemoNavbar/>
            <section className="section-profile-cover section-shaped my-0">
            {/* Circles background */}
            <div className="shape shape-style-1 shape-default alpha-4">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            </section>
            <Container style={{marginTop: "-25%", marginBottom: "5%"}} fluid-sm="true">
                <Row>
                    <DragList dataProducts={ofertas} history={this.props.history}/>
                </Row>
            </Container>
            <Footer/>
            </>
        );
    }
};
export default ProductBuyList;