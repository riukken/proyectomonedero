import React, { Component } from 'react';
import {
  Col, Row, Popover, PopoverBody
} from 'reactstrap';

import "./Product.css";

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clockIsOpen: false,
      selectedQuantity: 1,
      productPropierties: this.props.productPropierties
    };

    this.clockToggle = this.clockToggle.bind(this);
    this.plusOne = this.plusOne.bind(this);
    this.minusOne = this.minusOne.bind(this);
    
  };
  clockToggle() {
    this.setState({
      clockIsOpen: !this.state.clockIsOpen
    });
  }
  plusOne() {
    this.setState({
      selectedQuantity: (this.state.selectedQuantity === this.state.productPropierties.maxQuantity) ? (this.state.selectedQuantity) : (this.state.selectedQuantity+1)
    });
  }
  minusOne() {
    this.setState({
      selectedQuantity: (this.state.selectedQuantity === 1 ) ? (this.state.selectedQuantity) : (this.state.selectedQuantity-1) 
    });
  }

  render() {
    return (
      <div className="cardProduct">
        <img className="cardImage" src={this.state.productPropierties.img} alt={this.state.productPropierties.alt} />
        <label className="labelTitulo">
          <i>
            <svg className="logoProduct">
              <g>
                <path d="M 7 1 C 7 1.5523 7.6716 2 8.5 2 C 9.3284 2 10 1.5523 10 1 C 10 0.4477 9.3284 0 8.5 0 C 7.6716 0 7 0.4477 7 1 ZM 7 2 C 7 2 0.6875 2.9375 0 7 C 0 7 -0.1875 9.6875 3 11 C 3 11 4.6875 10.9375 6 9 C 6 9 8.3125 12.8125 11 9 C 11 9 12.1875 10.875 14 11 C 14 11 16.75 10.0625 17 7 C 17 7 16.375 3 10.0625 1.9375 C 10.0625 1.9375 8.5 4.25 7 2 ZM 1 11 L 3 17 L 14 17 L 16 11 C 16 11 13.125 13 11 11 C 11 11 8.875 13.375 6 11 C 6 11 3 13.125 1 11 Z" fill="#ffffff" />
              </g>
            </svg>
          </i>
          <font>{this.state.productPropierties.title}</font>
        </label>
        <i href="/"><i className="fa fa-clock-o" id="Popover1"></i></i>
        <div>
          <Popover placement="bottom" isOpen={this.state.clockIsOpen} target="Popover1" toggle={this.clockToggle}>
            <PopoverBody>Válido hasta: {this.state.productPropierties.expireDate}</PopoverBody>
          </Popover>
        </div>
        <div className="cardInfo">
          <Row>
            <Col md="4">
              <div className="cardQuantity">
                <i className="fa fa-angle-left quantityButton" onClick={this.minusOne} />
                <div className="quantityButton">{this.state.selectedQuantity}</div>
                <i className="fa fa-angle-right quantityButton" onClick={this.plusOne} />
              </div>
            </Col>
            <Col md="5">
              <p className="description">{this.state.productPropierties.descripcion}</p>
            </Col>
            <Col md="3">
              <h2 className="price">{this.state.productPropierties.precio} €</h2>
              {/* <div className="stars">
                <li>
                  <a href="/"><i className="fa fa-star"></i></a>
                  <a href="/"><i className="fa fa-star"></i></a>
                  <a href="/"><i className="fa fa-star"></i></a>
                  <a href="/"><i className="fa fa-star"></i></a>
                  <a href="/"><i className="fa fa-star-o"></i></a>
                </li>
              </div> */}
            </Col>
          </Row>
        </div>
      </div>
    );
  };
}
export default Product; 