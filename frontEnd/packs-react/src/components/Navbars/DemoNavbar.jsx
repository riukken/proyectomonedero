/*!

=========================================================
* Argon Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import { Link, Redirect } from "react-router-dom";
import logo from "assets/img/logo/logo.png";
// JavaScript plugin that hides or shows a component based on your scroll
//import Headroom from "headroom.js";
// reactstrap components
import {
  Button,
  UncontrolledCollapse,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  UncontrolledDropdown,
  Media,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

class DemoNavbar extends React.Component {
  constructor(props) {
    super(props)
    const sessionClient = window.localStorage.getItem("cliente");
    const sessionCompany = window.localStorage.getItem("empresa");


    this.state = {
      sessionStartedClient: sessionClient,
      sessionStartedCompany: sessionCompany
    };
    this.singOffCompany = this.singOffCompany.bind(this);
    this.singOffClient = this.singOffClient.bind(this);
  }
  singOffCompany() {
    window.localStorage.removeItem("empresa");
    // this.props.history.push({
    //   pathname: '/'
    // })
  }
  singOffClient() {
    window.localStorage.removeItem("cliente");
    // this.props.history.push({
    //   pathname: '/'
    // })
  }
  componentDidMount() {
    /*
    let headroom = new Headroom(document.getElementById("navbar-main"));
    // initialise
    headroom.init();
    */
  }
  render() {

    var navbar = (
      <header className="header-global">
        <Navbar
          style={{ color: "" }}
          className="navbar-dark bg-default navbar navbar-expand-lg"
          expand="lg"
        >
          <Container>

            <a href="/"><img
              alt="..."
              src={logo}
              width="90px"
              height="100px"
              style={{ position: "absolute",marginTop: "-50px", marginLeft: "-100px" }}
            /></a>

            <button className="navbar-toggler" id="navbar_global">
              <span className="navbar-toggler-icon" />
            </button>
            <UncontrolledCollapse navbar toggler="#navbar_global">
              <div className="navbar-collapse-header">
                <Row>
                  <Col className="collapse-brand" xs="6">
                    <span>Menu</span>
                  </Col>
                  <Col className="collapse-close" xs="6">
                    <button className="navbar-toggler" id="navbar_global">
                      <span />
                      <span />
                    </button>
                  </Col>
                </Row>
              </div>
              <Nav className="navbar-nav-hover align-items-lg-center" navbar>

                {/* <UncontrolledDropdown nav>
              <DropdownToggle nav>
                <i className="ni ni-collection d-lg-none mr-1" />
                <span className="nav-link-inner--text">Navegación</span>
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem to="/landing-page" tag={Link}>
                  Ofertas disponibles
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown> */}
              </Nav>


              <Nav className="align-items-lg-center ml-lg-auto" navbar>

                <NavItem className="d-none d-lg-block ml-lg-4">
                  <Button
                    className="btn-white btn-icon"
                    color="default"
                    href="/login-page"
                    size="lg"
                  >
                    <span className="btn-inner--icon mr-1">
                      <i className="fas fa-user"></i>
                    </span>
                    <span className="nav-link-inner--text ml-1">
                      Iniciar Sesión
                </span>
                  </Button>
                  <Button
                    className="btn-icon mb-3 mb-sm-0"
                    color="github"
                    href="/registerClient-page"
                    size="lg"
                  >
                    <span className="btn-inner--icon mr-1">
                      <i className="fas fa-user-plus"></i>
                    </span>
                    <span className="btn-inner--text">
                      <span className="text-warning mr-1"> Registro </span>
                    </span>
                  </Button>
                </NavItem>
              </Nav>
            </UncontrolledCollapse>
          </Container>
        </Navbar>
      </header>
    )

    var navbarClient = (

      <header className="header-global">
        {/*
          <Navbar
            className="navbar-main navbar-transparent navbar-light headroom"
            expand="lg"
            id="navbar-main"
          >
          */}
        <Navbar
          style={{ color: "" }}
          className="navbar-dark bg-default navbar navbar-expand-lg"
          expand="lg"
        >
          <Container>

          <a href="/"><img
              alt="..."
              src={logo}
              width="90px"
              height="100px"
              style={{ position: "absolute",marginTop: "-50px", marginLeft: "-100px" }}
            /></a>


            <button className="navbar-toggler" id="navbar_global">
              <span className="navbar-toggler-icon" />
            </button>
            <UncontrolledCollapse navbar toggler="#navbar_global">
              <div className="navbar-collapse-header">
                <Row>
                  <Col className="collapse-brand" xs="6">
                    <span>Menu</span>
                  </Col>
                  <Col className="collapse-close" xs="6">
                    <button className="navbar-toggler" id="navbar_global">
                      <span />
                      <span />
                    </button>
                  </Col>
                </Row>
              </div>
              <Nav className="navbar-nav-hover align-items-lg-center" navbar>

                <UncontrolledDropdown nav>
                  <DropdownToggle nav>
                    <i className="ni ni-collection d-lg-none mr-1" />
                    <span className="nav-link-inner--text">Navegación</span>
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem to="/profileClient-page" tag={Link}>
                      Perfil
                      </DropdownItem>
                    <DropdownItem to="/ProductList" tag={Link}>
                      Comprar ofertas
                     </DropdownItem>
                    <DropdownItem to="/listCompanies-page" tag={Link}>
                      Ofertas disponibles
                     </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>


              <Nav className="align-items-lg-center ml-lg-auto" navbar>

                <NavItem className="d-none d-lg-block ml-lg-4">
                  <Button
                    onClick={this.singOffClient}
                    className="btn-icon mb-3 mb-sm-0"
                    color="github"
                    href="/"
                    size="lg"
                  >
                    <span className="btn-inner--icon mr-1">
                      <i className="ni ni-curved-next"></i>
                    </span>
                    <span className="btn-inner--text">
                      <span className="text-warning mr-1"> Cerrar sesión </span>
                    </span>
                  </Button>
                </NavItem>
              </Nav>
            </UncontrolledCollapse>
          </Container>
        </Navbar>
      </header>

    )


    var navbarCompany = (

      <header className="header-global">
        {/*
          <Navbar
            className="navbar-main navbar-transparent navbar-light headroom"
            expand="lg"
            id="navbar-main"
          >
          */}
        <Navbar
          style={{ color: "" }}
          className="navbar-dark bg-default navbar navbar-expand-lg"
          expand="lg"
        >
          <Container>

          <a href="/"><img
              alt="..."
              src={logo}
              width="90px"
              height="100px"
              style={{ position: "absolute",marginTop: "-50px", marginLeft: "-100px" }}
            /></a>


            <button className="navbar-toggler" id="navbar_global">
              <span className="navbar-toggler-icon" />
            </button>
            <UncontrolledCollapse navbar toggler="#navbar_global">
              <div className="navbar-collapse-header">
                <Row>
                  <Col className="collapse-brand" xs="6">
                    <span>Menu</span>
                  </Col>
                  <Col className="collapse-close" xs="6">
                    <button className="navbar-toggler" id="navbar_global">
                      <span />
                      <span />
                    </button>
                  </Col>
                </Row>
              </div>
              <Nav className="navbar-nav-hover align-items-lg-center" navbar>

                <UncontrolledDropdown nav>
                  <DropdownToggle nav>
                    <i className="ni ni-collection d-lg-none mr-1" />
                    <span className="nav-link-inner--text">Navegación</span>
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem to="/profileCompany-page" tag={Link}>
                      Perfil
                      </DropdownItem>
                    <DropdownItem to="/productListCompany-page" tag={Link}>
                      Ofertas creadas
                      </DropdownItem>
                    <DropdownItem to="/newProduct-page" tag={Link}>
                      Crear oferta
                      </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>


              <Nav className="align-items-lg-center ml-lg-auto" navbar>

                <NavItem className="d-none d-lg-block ml-lg-4">
                  <Button
                    onClick={this.singOffCompany}
                    className="btn-icon mb-3 mb-sm-0"
                    color="github"
                    href="/"
                    size="lg"
                  >
                    <span className="btn-inner--icon mr-1">
                      <i className="ni ni-curved-next"></i>
                    </span>
                    <span className="btn-inner--text">
                      <span className="text-warning mr-1"> Cerrar sesión </span>
                    </span>
                  </Button>
                </NavItem>
              </Nav>
            </UncontrolledCollapse>
          </Container>
        </Navbar>
      </header>

    )

    var prueba

    if (this.state.sessionStartedClient) {
      prueba = navbarClient
    }
    else if (this.state.sessionStartedCompany) {
      prueba = navbarCompany
    }
    else {
      prueba = navbar
    }


    return (
      <>
        {prueba}
      </>

    );
  }
}

export default DemoNavbar;
